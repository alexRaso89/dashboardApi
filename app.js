var express = require('express');
var app = express();
const bodyParser = require('body-parser');
const http = require('http');  
const mongo = require('mongodb');
var mongoXlsx = require('mongo-xlsx');
var mime = require('mime');
var fs = require('fs');
app.use(function (req, res, next) {
    
        // Website you wish to allow to connect
        //res.setHeader('Access-Control-Allow-Origin', 'http://localhost:4200');
        res.setHeader('Access-Control-Allow-Origin', '*');
        // Request methods you wish to allow
        res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
  
        res.header("Access-Control-Allow-Headers", "X-Requested-With, Content-Type");
        next();
    });
var MongoClient = require('mongodb').MongoClient;
    var url = "mongodb://localhost:27017/apiDb";
    
    

app.use(express.static('.'));
app.use(bodyParser.json());  
app.use(bodyParser.urlencoded({ extended: false }));
app.get('/exportAll', (req, res) => {  
    var data="";
       /* Generate automatic model for processing (A static model should be used) */
       modello=[
        {
          "displayName": "id",
          "access": "_id",
          "type": "string"
        },
        {
          "displayName": "api",
          "access": "api",
          "type": "string"
        },
        {
            "displayName": "microservizio",
            "access": "microservizio",
            "type": "string"
          },
          {
            "displayName": "swagger",
            "access": "swagger",
            "type": "string"
          },
          {
            "displayName": "sistema",
            "access": "sistema",
            "type": "string"
          },
          {
            "displayName": "metodo",
            "access": "metodo",
            "type": "string"
          },
          {
            "displayName": "descrizione",
            "access": "descrizione",
            "type": "string"
          }
       
        ];
        modello1=[
            {
                "displayName": "api",
                "access": "api",
                "type": "string"
              },
                {
                    "displayName": "ambito",
                    "access": "ambito",
                    "type": "string"
                  },
                  {
                    "displayName": "epics",
                    "access": "epics",
                    "type": "string"
                  },
        ];
    console.log("entro");
    MongoClient.connect(url, function(err, db) {
        if (err) throw err;
        var dbo = db.db("apiDb");
        dbo.collection("apiList").find({}).toArray(function(err, result) {
            if (err) throw err;
            
            db.close();
            data=result;
            var applicazione=[];
            data.forEach(function(el) {
                console.log(el);
                for (var index = 0; index < el.applicazione.length; index++) {
                    el.applicazione[index]["api"]=el.api;
                    applicazione.push(el.applicazione[index]);
                   console.log(el.applicazione[index]); 
                }
            
                

            }, this);
                /* Generate Excel */
        var mod=mongoXlsx.mongoData2XlsxData(data, modello);
        var mod1=mongoXlsx.mongoData2XlsxData(applicazione, modello1)
        console.log(applicazione);
        console.log(mod1);
        mongoXlsx.mongoData2XlsxMultiPage([mod,mod1],["api","apiToEpics"], function(err,data){
            console.log(data);
            //var file = __dirname + '/upload-folder/dramaticpenguin.MOV';
            //res.download(data.fullPath);
            var mimetype = mime.lookup(data.fullPath);
            
              res.setHeader('Content-disposition', 'attachment; filename=' + data.fileName);
              res.setHeader('Content-type', mimetype);
            
              var filestream = fs.createReadStream(data.fullPath);
              filestream.pipe(res);
              fs.unlinkSync(data.fullPath);
        });
        

        /*mongoXlsx.mongoData2Xlsx(data, modello, function(err, data) {
            console.log(data);
            console.log('File saved at:', data.fullPath); 
            });*/



          });
      });

 



});
app.get('/data', (req, res) => {  
    MongoClient.connect(url, function(err, db) {
        if (err) throw err;
        var dbo = db.db("apiDb");
        dbo.collection("apiList").find({}).toArray(function(err, result) {
            if (err) throw err;
            console.log(result);
            db.close();
            res.send( result);
          });
      });
});
app.delete('/api', function(req, res){  
    console.log(req.body[0]);
    
    //insert_in_db(obj);
    
    
    MongoClient.connect(url, function(err, db) {
        if (err) throw err;
        var dbo = db.db("apiDb");
        var arr=[]
        req.body.forEach(function(element) {
            arr.push(new mongo.ObjectID(element) );
        }, this);
       var myquery = {_id:{'$in':arr}};
        //var myquery = { '_id':req.body[0]};
        console.log(myquery);
        dbo.collection("apiList").deleteMany(myquery, function(err, obj) {
          if (err) throw err;
          console.log(obj.result.n + " document(s) deleted");
          db.close();
          res.send('deleted');
        });
      });


});
app.post('/api', function(req, res){  
    console.log(req.body);
    var obj=req.body;
    insert_in_db(obj);
    
    res.send('saved');
});
app.patch('/epics/:id', function(req, res){ 
    console.log(req.body);

     MongoClient.connect(url, function(err, db) {
        if (err) throw err;
        var dbo = db.db("apiDb");
        var myquery = { _id: new mongo.ObjectID(req.params.id) };
        var newvalues = { $push: { applicazione: { $each: req.body.applicazione } } } ;
        dbo.collection("apiList").updateOne(myquery, newvalues, function(err, res) {
          if (err) throw err;
          console.log("1 document updated");
          db.close();
        });
      });


    res.send('saved');
});
app.patch('/api', function(req, res){  
    console.log(req.body);
    var obj=req.body;
    
    MongoClient.connect(url, function(err, db) {
        if (err) throw err;
        var dbo = db.db("apiDb");
        var myquery = { _id: new mongo.ObjectID(req.body._id) };
        var newvalues = { $set: {'api': req.body.api, 'microservizio': req.body.microservizio, 'swagger': req.body.swagger,'sistema':req.body.sistema,'metodo':req.body.metodo,'descrizione':req.body.descrizione, 'esterno':req.body.esterno } };
        dbo.collection("apiList").updateOne(myquery, newvalues, function(err, res) {
          if (err) throw err;
          console.log("1 document updated");
          db.close();
        });
      });

    res.send('saved');
});
app.get('/examples/:project/:func', require('./examples'));
const port = process.env.PORT || '3000';  
app.set('port', port); 

//server
const server = http.createServer(app);

//listen on the port
server.listen(port, () =>{
     console.log('API running on localhost:' + port)
     MongoClient.connect(url, function(err, db) {
        var dbo = db.db("apiDb");
        dbo.createCollection("apiList", function(err, res) {
          if (err) throw err;
          console.log("Collection created!");
          db.close();
        });
      });
       
    }); 

function insert_in_db(data){
    //var obj={item:[]}
    //obj.item=data;
    MongoClient.connect(url, function(err, db) {
        if (err) throw err;
        var dbo = db.db("apiDb");
      
        dbo.collection("apiList").insertOne(data, function(err, res) {
          if (err) throw err;
          console.log("1 document inserted");
          db.close();
        });
      });
}

function getData(){
    
    

}
